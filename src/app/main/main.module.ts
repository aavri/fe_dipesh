import { NgModule,CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { SharedModule } from './../shared/shared.module';

import { BranchesComponent } from './branches/branches.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    SharedModule
  ],
  exports: [BranchesComponent],
  declarations: [BranchesComponent]
  // schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class MainModule { }
