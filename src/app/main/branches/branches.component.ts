import { Component, OnInit, ViewChild } from '@angular/core';
import { BranchesService } from '../../shared/services/branches.service';
import { ModalComponent } from '../../shared/components/modal/modal.component';

@Component({
  selector: 'app-branches',
  templateUrl: './branches.component.html',
  styleUrls: ['./branches.component.css']
})

export class BranchesComponent implements OnInit {Branch
 
  /* Variables declaration with type */
  branchListArr:any[] = [];
  brandName:string;
  searchText:string;
  branchData:any;
  @ViewChild('branchDetail') branchDetail: ModalComponent;
  showModal:boolean = false;

  constructor(private bservice: BranchesService) { }
  
  ngOnInit() {
  	this.getAllBranches();
  }

  /* Function to  get all the branches from API call */
  getAllBranches() {
  	this.bservice.getBranchesList().subscribe((res:any) => {
  		console.log(res);
  		this.branchListArr = res.data[0].Brand[0].Branch;
  		this.brandName = res.data[0].Brand[0].BrandName;
  		console.log(this.branchListArr);
  	})

    let number1 : number = this.getSearchQuery('foo');
  }

  getSearchQuery(e) {
  	this.searchText = e;
  	console.log("The search query is " + this.searchText)
    return this.searchText;
  }

  showBranchDetail(item) {
    console.log("The Branch to show is ", item);
    this.branchData = item;
    this.showModal = true;
    this.branchDetail.openModal();
  }

  closeModal() {
    this.branchDetail.closeModal();
  }
}
