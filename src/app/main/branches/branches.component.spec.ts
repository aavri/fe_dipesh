import { async, ComponentFixture, TestBed, inject } from '@angular/core/testing';
import { SharedModule } from '../../shared/shared.module';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { BranchesComponent } from './branches.component';
import { BranchesService } from '../../shared/services/branches.service';

describe('BranchesComponent', () => {
  let component: BranchesComponent;
  let fixture: ComponentFixture<BranchesComponent>;
  let service: BranchesService;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BranchesComponent ],
      imports: [SharedModule, HttpClientModule, FormsModule],
      providers: [BranchesService] 
    })
    .compileComponents();
    service = TestBed.get(BranchesService);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BranchesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('it should get all the branches', () => {
    component.getAllBranches();
    fixture.detectChanges();
    service.getBranchesList().subscribe(res => {
      expect(component.branchListArr.length).toBeGreaterThan(0);
    })
  });
});
