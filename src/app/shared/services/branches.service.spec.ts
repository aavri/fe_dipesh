import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { BranchesService } from './branches.service';
import { HttpClientModule } from '@angular/common/http';

describe('BranchesService', () => {
  
  let service:BranchesService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
  		TestBed.configureTestingModule({
	      imports: [HttpClientTestingModule],
	      providers: [BranchesService]
	    });

	    service = TestBed.get(BranchesService); 
	    httpMock = TestBed.get(HttpTestingController); 
  });

  afterEach(() => {
  	httpMock.verify();
  })

  it('Should retrive all the branches from API via GET',() => {

  	const branches:[] = [
  		{ 
  			name: 'TADCASTER',
  			contactInfo: [
  				{contactType:'Phone', ContactContent:'+44-1937835809'},
  				{contactType:'Fax', ContactContent:'+44-1937835809'}
  			],
  			postaladdress: { 
  				AddressLine:['HALIFAX BRANCH 24 BRIDGE STREET'],
  				TownName:'TADCASTER',
  				CountrySubDivision:['NORTH YORKSHIRE'],
  				Country:'GB',
  				PostCode:'LS24 9AL'
  			} 
  		},
  		{ 
  			name : 'WALTON VALE',
  			contactInfo: [
  				{contactType:'Phone', ContactContent:'+44-1937835809'},
  				{contactType:'Fax', ContactContent:'+44-1937835809'}
  			],
  			postaladdress: { 
  				AddressLine:['HALIFAX BRANCH 78-80 WALTON VALE'],
  				TownName:'LIVERPOOL',
  				CountrySubDivision:['MERSEYSIDE'],
  				Country:'GB',
  				PostCode:'L9 2BU'
  			} 
  		}
  	]


  	service.getBranchesList().subscribe(res => {
  		expect(res.length).toBe(2);
  		expect(res).toEqual(branches);
  	});

  	const request = httpMock.expectOne(service.serviceUrl);
  	expect(request.request.method).toBe('GET');
  	request.flush(branches);
  })
});
