import { Injectable }  from '@angular/core';
import { environment } from '../../../environments/environment';
import { HttpClient }  from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable()
export class BranchesService {
  serviceUrl = environment.baseUrl+'branches';
  constructor(private httpService: HttpClient) { }

  getBranchesList() {
  	console.log('I am in service');
    return this.httpService.get(this.serviceUrl).pipe(map(res => {
        return res;
      })
    )
  }
}
