import { NgModule,CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { SearchBarComponent } from './components/search-bar/search-bar.component';
import { FilterPipe } from './pipes/filter.pipe';
import { BranchesService } from './services/branches.service';
import { ModalComponent } from './components/modal/modal.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule
  ],
  exports: [SearchBarComponent, FilterPipe, ModalComponent],
  declarations: [SearchBarComponent, FilterPipe, ModalComponent],
  providers: [BranchesService]
  // schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class SharedModule { }
